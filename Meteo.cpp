
// Meteo
// https://gitlab.com/lowswaplab/meteo
// Copyright © 2020 Low SWaP Lab lowswaplab.com

#include "Meteo.hpp"
#include <math.h>

// https://en.wikipedia.org/wiki/Heat_index
float LowSWaPLab::heatindex(const float temp, const float hum)
  {
  // formula only valid for certain temperature/humidity range
  if (temp < 26.7 || hum < 40)
    return(-1);

  const float c1=-8.78469475556;
  const float c2=1.61139411;
  const float c3=2.33854883889;
  const float c4=-0.14611605;
  const float c5=-0.012308094;
  const float c6=-0.0164248277778;
  const float c7=0.002211732;
  const float c8=0.00072546;
  const float c9=-0.000003582;

  return(c1 + c2*temp + c3*hum + c4*temp*hum + c5*temp*temp +
         c6*hum*hum + c7*temp*temp*hum + c8*temp*hum*hum +
         c9*temp*temp*hum*hum);
  }

// https://en.wikipedia.org/wiki/Humidex
float LowSWaPLab::humidex(const float temp, const float dp)
  {
  float expIndex;

  expIndex = 5417.753 * ((1.0 / 273.16) - (1.0 / (dp + 273.16)));
  return(temp + 0.5555 * (6.11 * exp(expIndex) - 10));
  }

String LowSWaPLab::humidexMessage(const float humidex)
  {
  if (humidex > 45)
    return(F("dangerous; heat stroke quite possible"));
  else if (humidex >= 40)
    return(F("great discomfort, avoid exertion"));
  else if (humidex >= 30)
    return(F("some discomfort"));
  else if (humidex >= 20)
    return(F("little to no discomfort"));

  return(F(""));
  }

// https://en.wikipedia.org/wiki/Thermal_comfort
String LowSWaPLab::humidityWarning(const float humidity)
  {
  if (humidity < 30)
    return(F("dry"));
  else if (humidity >= 50)
    return(F("humid"));

  return(F(""));
  }

String LowSWaPLab::heatindexWarning(const float heatindex)
  {
  // https://www.weather.gov/ama/heatindex
  if (heatindex >= 51.67)
    return(F("extreme danger"));
  else if (heatindex >= 39.44)
    return(F("danger"));
  else if (heatindex >= 32.22)
    return(F("extreme caution"));
  else if (heatindex >= 26.66)
    return(F("caution"));

  return(F(""));
  }

String LowSWaPLab::pressureMessage(const float pressure)
  {
  if (pressure < 990)       // 29.25 inHg
    return(F("stormy"));
  else if (pressure < 1007) // 29.75 inHg
    return(F("change"));

  return(F("clear"));
  }

