
# Meteo

Meteo is a set of weather-related functions.



# Platforms

Meteo has been ported to the Arduino and Linux development
environments.



# Source Code

[Meteo Git Repository](https://gitlab.com/lowswaplab/meteo).



# Contribution

If you find this software useful, please consider financial support
for future development via
[PayPal](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=GY9C5FN54TCKL&source=url).



# Author

[Low SWaP Lab](https://www.lowswaplab.com/)



# Copyright

Copyright © 2020 Low SWaP Lab [lowswaplab.com](https://www.lowswaplab.com/)

