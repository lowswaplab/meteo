
// Meteo
// https://gitlab.com/lowswaplab/meteo
// Copyright © 2020 Low SWaP Lab lowswaplab.com

#ifndef METEO_HPP
  #define METEO_HPP

  #ifdef ARDUINO
    #include <Arduino.h>
  #endif

  #if defined(HAVE_OSX) || defined (HAVE_LINUX)
    #include <string>
    #define String std::string
    #define F(x) (x)
  #endif

  /*! \brief Low SWaP Lab namespace
   *
   *  Low SWaP Lab software can be found at lowswaplab.com
   */
  namespace LowSWaPLab
    {
    //! Calculate heat index (https://en.wikipedia.org/wiki/Heat_index).
    //! This algorithm only works if the air temperature ≥ 26.7°C and
    //! humidity ≥ 40%.
    //! \param temp Ambient air temperature in °C.
    //! \param hum Ambient air humidity in %.
    //! \return Heat index in °C if temp ≥ 26.7°C and hum ≥ 40%,
    //!         otherwise -1.
    float heatindex(const float temp, const float hum);

    //! Calculate humidex (https://en.wikipedia.org/wiki/Humidex).
    //! \param temp Ambient air temperature in °C.
    //! \param hum Ambient air dew point in °C.
    //! \return Humidex.
    float humidex(const float temp, const float dp);

    //! Generate a message based on humidex.
    //! \param humidex Humidex.
    //! \return Message if too humid, otherwise zero length string.
    String humidexMessage(const float humidex);

    //! Generate a warning message based on humidity
    //! (https://en.wikipedia.org/wiki/Thermal_comfort).
    //! \param humidity Ambient air humidity in %.
    //! \return Warning message if too dry or too humid, otherwise zero
    //!         length string.
    String humidityWarning(const float humidity);

    //! Generate a warning message based on heat index.
    //! \param heatindex Heat index in °C.
    //! \return Warning message if too hot/humid, otherwise zero length
    //!         string.
    String heatindexWarning(const float heatindex);

    //! Generate a message based on pressure.
    //! \param pressure Ambient air pressure in hPa.
    //! \return Message if weather is clear, changing, or stormy.
    String pressureMessage(const float pressure);
    }

#endif

